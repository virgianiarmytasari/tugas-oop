@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf
    <label for="fnama">Fist name:</label><br><br>
    <input type="text" name="fnama"><br><br>

    <label for="lnama">Last name:</label><br><br>
    <input type="text" name="lnama"><br><br>

    <label for="gender">Gender</label><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>

    <label for="nasionality">Nasionality:</label><br><br>
    <select name="nasionality">
        <option value="indonesia">Indonesia</option>
        <option value="singapura">Singapura</option>
        <option value="vietnam">Vietnam</option>
        <option value="malaysia">Malaysia</option>
        <option value="kamboja">Kamboja</option>
        <option value="brunei">Brunnei Darussalam</option>
        <option value="korea">Korea</option>
        <option value="jepang">Jepang</option>
        <option value="china">China</option>

    </select><br><br>
    <label for="bahasa">Language Spoken:</label><br><br>
    <input type="checkbox" name="bahasa"> Indonesia<br>
    <input type="checkbox" name="bahasa"> English<br>
    <input type="checkbox" name="bahasa"> Other<br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea>
    <br>
    <input type="Submit" value="Sign Up">
</form>
@endsection

    
