<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.biodata');
    }

    public function welcome(Request $request)
    {
        $Fullnama = $request['fnama'] ." ". $request['lnama'];

        return view('halaman.home', ['Fullnama' => $Fullnama]);
    }
}
